#include <iostream>
using namespace std;

#include "arbol_binario.h"
#include "grafo.h"

arbol_binario::arbol_binario(){}

/* Función para añadir el nodo al árbol */
void arbol_binario::add_nodo(Nodo *arbol, int dato) {

    if (this->raiz_arbol == NULL) { 
        arbol->next_izq = NULL;
        arbol->next_der = NULL;
        arbol->previous = NULL;
        arbol->dato = dato;

        this->raiz_arbol = arbol;

        cout << "Se ha guardado como raíz del árbol la variable [" << dato << "]." << endl << endl; 
    
    }else{
        if (arbol->dato > dato) {
            if (arbol->next_izq == NULL) {
                Nodo *new_directriz = new Nodo();
                new_directriz->next_izq = NULL;
                new_directriz->next_der = NULL;
                new_directriz->previous = arbol;
                new_directriz->dato = dato;
                arbol->next_izq = new_directriz;
                cout << "Se ha guardado la variable [" << dato << "] en el árbol." << endl << endl; 
            }else {
                add_nodo(arbol->next_izq, dato);
            }

        }else {
            if (arbol->dato < dato) {
                if (arbol->next_der == NULL) {
                    Nodo *new_directriz = new Nodo();
                    new_directriz->next_izq = NULL;
                    new_directriz->next_der = NULL;
                    new_directriz->previous = arbol;
                    new_directriz->dato = dato;
                    arbol->next_der = new_directriz;
                    cout << "Se ha guardado la variable [" << dato << "] en el árbol." << endl << endl; 
                }else {
                    add_nodo(arbol->next_der, dato);
                }        
            }else {
                cout << "Su variable ya esta en el árbol. " << endl << endl;
            }
        }
    }
}


/* Función para buscar un nodo en el árbol */
void arbol_binario::busqueda_nodo(Nodo *arbol, int dato, int indicar) {
    
    if (arbol == NULL){
        cout << "El árbol no contiene el valor seleccionado. " << endl << endl;
        return;
    }

    if (arbol->dato > dato) {
        busqueda_nodo(arbol->next_izq, dato, indicar);

    }else if (arbol->dato < dato) {
        busqueda_nodo(arbol->next_der, dato, indicar);

    }else if (arbol->dato == dato){
        if (indicar == 1){
            change_nodo(arbol, dato);
        }else if (indicar == 2){
            remove_nodo(arbol, dato);
        }
    }
}


/* Función para cambiar el dato de un nodo */
void arbol_binario::change_nodo(Nodo *arbol, int dato) {
    remove_nodo(arbol, dato);

    cout << "Ingrese el nuevo dato que se añadira: ";
    cin >> dato;

    clear();

    add_nodo(arbol, dato);
}


/* Función para eliminar un nodo */
void arbol_binario::remove_nodo(Nodo *arbol, int dato){
    if (arbol != NULL){
        if (dato < arbol->dato) {
            remove_nodo(arbol->next_izq,dato);
        }else if (dato > arbol->dato) {
            remove_nodo(arbol->next_der,dato);    
        }else {
            if (arbol->next_izq == NULL && arbol->next_der == NULL){
                if (arbol->previous->next_izq != NULL) {
                    if (arbol->previous->next_izq->dato == dato){
                        arbol->previous->next_izq = NULL;
                        arbol = NULL;
                        return;
                    }
                }
                if (arbol->previous->next_der != NULL){
                    if (arbol->previous->next_der->dato == dato){
                        arbol->previous->next_der = NULL;
                        arbol = NULL;
                        return;
                    }
                }       
            }else if (arbol->next_izq != NULL && arbol->next_der != NULL) {
                eliminar_nodos(arbol, dato);
                
            }else if (arbol->next_izq != NULL){
                if (arbol->previous->next_izq != NULL) {
                    if (arbol->previous->next_izq->dato == dato){
                        arbol->previous->next_izq = NULL;
                        arbol->previous->next_izq = arbol->next_izq;

                        arbol->next_izq->previous = NULL;
                        arbol->next_izq->previous = arbol->previous;

                        arbol = NULL;
                        return;
                    }
                }
                if (arbol->previous->next_der != NULL){
                    if (arbol->previous->next_der->dato == dato){
                        
                        arbol->previous->next_der = NULL;
                        arbol->previous->next_der = arbol->next_izq;

                        arbol->next_izq->previous = NULL;
                        arbol->next_izq->previous = arbol->previous;

                        arbol = NULL;
                        return;
                    }
                }

            }else if (arbol->next_der != NULL){
               if (arbol->previous->next_izq != NULL) {
                    if (arbol->previous->next_izq->dato == dato){
                        arbol->previous->next_izq = NULL;
                        arbol->previous->next_izq = arbol->next_der;

                        arbol->next_der->previous = NULL;
                        arbol->next_der->previous = arbol->previous;

                        arbol = NULL;
                        return;
                    }
                }if (arbol->previous->next_der != NULL){
                    if (arbol->previous->next_der->dato == dato){
                        
                        arbol->previous->next_der = NULL;
                        arbol->previous->next_der = arbol->next_der;

                        arbol->next_der->previous = NULL;
                        arbol->next_der->previous = arbol->previous;

                        arbol = NULL;
                        return;
                    }
                }
            }
        }
    }else {
        cout << "La información no se encuentra en el árbol. " << endl << endl;
    }
}


/* Función para elimar nodos con 2 hijos los nodos del arbol */
void arbol_binario::eliminar_nodos(Nodo *arbol, int dato){
    Nodo *indice;

    if (this->raiz_arbol->dato >= dato) {
        indice = arbol->next_der;

        while (indice->next_izq != NULL){
            indice = indice->next_izq;
        }

        if (arbol->previous->next_izq != NULL) {
            if (arbol->previous->next_izq->dato == dato){
                arbol->previous->next_izq = NULL;
                arbol->previous->next_izq = indice;

                if (arbol->next_der->dato == indice->dato){
                    indice->next_izq = arbol->next_izq;
                    return;
                }

                indice->previous->next_izq = NULL;
                indice->previous = arbol->previous;
                indice->next_izq = arbol->next_izq;
                indice->next_der = arbol->next_der;
                return;
            }
        }
        if (arbol->previous->next_der != NULL){
            if (arbol->previous->next_der->dato == dato){
                arbol->previous->next_der = NULL;
                arbol->previous->next_der = indice;

                if (arbol->next_der->dato == indice->dato){
                    indice->next_izq = arbol->next_izq;
                    return;
                }
                indice->previous->next_izq = NULL;
                indice->previous = arbol->previous;
                indice->next_izq = arbol->next_izq;
                indice->next_der = arbol->next_der;
                return;
            }
        }
    }else {
        indice = arbol->next_izq;

        while (indice->next_der != NULL){
            indice = indice->next_der;
        }

        if (arbol->previous->next_izq != NULL) {
            if (arbol->previous->next_izq->dato == dato){
                arbol->previous->next_izq = NULL;
                arbol->previous->next_izq = indice;

                if (arbol->next_izq->dato == indice->dato){
                    indice->next_der = arbol->next_der;
                    return;
                }

                indice->previous->next_der = NULL;
                indice->previous = arbol->previous;
                indice->next_izq = arbol->next_izq;
                indice->next_der = arbol->next_der;
                return;
            }
        }
        if (arbol->previous->next_der != NULL) {
            if (arbol->previous->next_der->dato == dato){
                arbol->previous->next_der = NULL;
                arbol->previous->next_der = indice;

                if (arbol->next_izq->dato == indice->dato){
                    indice->next_der = arbol->next_der;
                    return;
                }

                indice->previous->next_der = NULL;
                indice->previous = arbol->previous;
                indice->next_izq = arbol->next_izq;
                indice->next_der = arbol->next_der;
                return;
            }
        }
    }           
}


/* Función para mostrar todos los nodos del arbol */
void arbol_binario::mostrar_arbol(Nodo *arbol, int dato) {

    //Mostrar en preorden
    if (dato == 1) {
        if (arbol != NULL) {
            cout << "[" << arbol->dato << "]  ";
            mostrar_arbol(arbol->next_izq, dato);
            mostrar_arbol(arbol->next_der, dato);
        }


    //Mostrar en inorden
    }else if (dato == 2) {
        if (arbol != NULL) {
            mostrar_arbol(arbol->next_izq, dato);
            cout << "[" << arbol->dato << "]  ";
            mostrar_arbol(arbol->next_der, dato);
        }


    //Mostrar en postorden
    }else if (dato == 3) {
        if (arbol != NULL) {
            mostrar_arbol(arbol->next_izq, dato);
            mostrar_arbol(arbol->next_der, dato);
            cout << "[" << arbol->dato << "]  ";
        }


    }else {
        if (dato != 1 || dato != 2 || dato !=3) {
            cout << "Opción distinta de [1], [2] y [3]. ";
        }else if (this->raiz_arbol == NULL) {
            cout << "Árbol vacío. ";
        }
    }
}


/* Función para crear la imagen con el grafo junto al archivo .txt*/
void arbol_binario::crear_grafo(Nodo *arbol) {
    grafo *generar_grafo = new grafo(arbol);
}


/* Función para limpiar la terminal */
void arbol_binario::clear() {
    cout << "\x1B[2J\x1B[H";
}
#ifndef ARBOL_BINARIO_H
#define ARBOL_BINARIO_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    int dato;
    struct _Nodo *next_izq;
    struct _Nodo *next_der;
    struct _Nodo *previous;
} Nodo;

class arbol_binario {

    private:
        Nodo *raiz_arbol = NULL;

    public:
        arbol_binario();

        void add_nodo(Nodo *arbol, int dato);

        void busqueda_nodo(Nodo *arbol, int dato, int indicar);
        
        void change_nodo(Nodo *arbol, int dato);

        void remove_nodo(Nodo *arbol, int dato);

        void mostrar_arbol(Nodo *arbol, int dato);

        void crear_grafo(Nodo *arbol);

        void eliminar_nodos(Nodo *arbol, int dato);
        
        void clear();

};
#endif
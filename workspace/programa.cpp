#include<iostream>
using namespace std;

#include "arbol_binario.h"

/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "============= Menu =============" << endl;
    cout << "Agregar un nodo  ___________ [1]" << endl;
    cout << "Cambiar una variable _______ [2]" << endl;
    cout << "Eliminar una variable  _____ [3]" << endl;
    cout << "Mostrar el arbol ___________ [4]" << endl;
    cout << "Crear 'Grafo' ______________ [5]" << endl;
    cout << "Salir del programa _________ [0]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> opt;

    clear();

    return opt;
}

/* Función para el preguntar dato al usuario */
int solicitar_dato (int dato) {
    cout << "======== Ingresar ========" << endl;
    cout << "Dato: ";
    cin >> dato;

    clear();

    return dato;
}

/* Función para el indicar el metodo de mostrar */
int solicitar_opcion_mostrar (int dato) {
    cout << "============= Menu =============" << endl;
    cout << "Mostrar en preorden ________ [1]" << endl;
    cout << "Mostrar en inorden _________ [2]" << endl;
    cout << "Mostrar en postorden  ______ [3]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> dato;

    clear();

    return dato;
}

/* Función main */
int main(){
    string option = "\0";
    int dato = 0;

    arbol_binario *arbol = new arbol_binario();
    Nodo *arbol_b = new Nodo();

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            dato = solicitar_dato(dato);
            arbol->add_nodo(arbol_b, dato);
        }

        if (option == "2") {
            dato = solicitar_dato(dato);
            arbol->busqueda_nodo(arbol_b, dato, 1);
        }

        if (option == "3") {
            dato = solicitar_dato(dato);
            arbol->busqueda_nodo(arbol_b, dato, 2);
        }

        if (option == "4") {
            dato = solicitar_opcion_mostrar(dato);
            arbol->mostrar_arbol(arbol_b, dato);
            cout << endl << endl;
        }

        if (option == "5") {
            arbol->crear_grafo(arbol_b);
        }
    }

    return 0;
}
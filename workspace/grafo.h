#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
using namespace std;

#include "arbol_binario.h"

class grafo {

    private:

    public:
        grafo(Nodo *nodo);

        void recorrer_arbol(Nodo *arbol, ofstream &archivo);

};
#endif
#include <iostream>
#include<fstream>
using namespace std;

#include "grafo.h"
#include "arbol_binario.h"

grafo::grafo(Nodo *nodo){
    ofstream fp;
            
    /* abre archivo */
    fp.open ("grafo.txt");

    fp << "digraph G {" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
                
    recorrer_arbol(nodo, fp);

    fp << "}" << endl;

    /* cierra archivo */
    fp.close();
                    
    /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
    /* visualiza el grafo */
    system("eog grafo.png &");
}

void grafo::recorrer_arbol(Nodo *arbol, ofstream &archivo) {           
    if (arbol != NULL) {
        if (arbol->next_izq != NULL) {
            archivo << "\n" << arbol->dato << "->" << arbol->next_izq->dato << ";" << endl;

        } else {      
            archivo << "\n" << '"' << arbol->dato << "i" << '"' << " [shape=point];";
            archivo << "\n" << arbol->dato << "->" << '"' << arbol->dato << "i" << '"';
        }
                    
        if (arbol->next_der != NULL) {
            archivo << "\n" << arbol->dato << "->" << arbol->next_der->dato << ";" << endl;

        } else {      
            archivo << "\n" << '"' << arbol->dato << "d" << '"' << " [shape=point];";
            archivo << "\n" << arbol->dato << "->" << '"' << arbol->dato << "d" << '"';
        }

        recorrer_arbol(arbol->next_izq, archivo);
        recorrer_arbol(arbol->next_der, archivo); 
    }
}